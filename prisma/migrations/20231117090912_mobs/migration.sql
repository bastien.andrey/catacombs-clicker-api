-- CreateTable
CREATE TABLE "Mobs" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "hp" INTEGER NOT NULL,
    "attack" INTEGER NOT NULL,
    "cooldown" INTEGER NOT NULL
);
