import { Elysia } from "elysia";
import { swagger } from "@elysiajs/swagger";
import { configureMobsRoutes } from "./routes/MobsRoute";

const app = new Elysia();

app
  .use(swagger({
    path: "/v1/swagger"
  }))
  .get("/", () => `Welcome to Bun Elysia`)
  .group("/mobs", configureMobsRoutes)
  .listen(3000);

console.log(`🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`);