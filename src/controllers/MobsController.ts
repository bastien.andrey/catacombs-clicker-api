import { t } from "elysia";
import { PrismaClient } from "@prisma/client";

const db = new PrismaClient();

export const mobsController = {
    getMobs: async ({ query }) => {
        return db.mobs.findMany({
            where: {
                name: {
                    contains: query.title
                }
            }
        });
    },

    createMob: async ({ body, set }) => {
        await db.mobs.create({
            data: {
                id: body.id,
                name: body.name,
                hp: body.hp,
                attack: body.attack,
                cooldown: body.cooldown,
            }
        });
        set.status = 201;
        return `Data ${body.name} successfully created!`;
    },

    validateCreateMob: t.Object({
        id: t.String(),
        name: t.String(),
        hp: t.Integer(),
        attack: t.Integer(),
        cooldown: t.Integer(),
    })
};