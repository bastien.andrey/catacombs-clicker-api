import { Elysia, t } from "elysia";
import { mobsController } from "../controllers/MobsController";

export function configureMobsRoutes(app: Elysia) {
    return app
        .get("/", mobsController.getMobs)
        .guard({ body: mobsController.validateCreateMob }, (guardApp) =>
            guardApp.post("/", mobsController.createMob)
        );
}